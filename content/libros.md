+++
title = "Libros y papers"
weight = 2
+++
# Libros y papers
Algunas lecturas que ayudan con los ramos y cómo conseguirlas.

## Papers
Puedes conseguir casi cualquier paper (si tienes un enlace o *DOI*) usando **scihub**: [sci-hub](http://tool.yovisun.com/scihub/).

## Libros
{{< hint info >}}
**Hay varias alternativas de descarga del mismo libro**, por si un enlace deja de funcionar. Prefiere el primero.
{{< /hint >}}

- Charles H. Lehmann. **Álgebra** (4ta ed)
  - [archive.org](https://archive.org/details/LgebraCharlesH.Lehmann1ed)
  - [academia.edu](https://www.academia.edu/36161290/A_lgebra_Charles_H._Lehmann_1ed.PDF)
- James Stewart. **Precálculo: Matemáticas para el cálculo** (6ta ed)
  - [ftp1.unimeta.edu.co](http://ftp1.unimeta.edu.co/calculus/multivariable/books/precalculo_-_matematicas_para_el_calculo-1.pdf)
  - [academia.edu](https://www.academia.edu/36667198/PREC%C3%81LCULO_MATEM%C3%81TICAS_PARA_EL_C%C3%81LCULO)
  - [academia.edu](https://www.academia.edu/34403504/PREC%C3%81LCULO_MATEM%C3%81TICAS_PARA_EL_C%C3%81LCULO_6e)

## Cuentas
Si necesitas conseguir algo de alguna página, estas cuentas te pueden servir:

| Página       | Usuario                     | Contraseña    |
|--------------|-----------------------------|---------------|
| academia.edu | `academia14@mailinator.com` | `bugmenot`    |
| academia.edu | `qprdnwqc@sharklasers.com`  | `bugmenot`    |
| scribd.com   | `asfdssafsdf@yopmail.com`   | `asdfasdfasd` |

Puedes tener suerte para acceder a scribd.com haciendo click en [este misterioso link](https://www.google.com/search?q=site:pastebin.com+"scribd"+-bin&hl=en&source=lnt&tbs=qdr:y).

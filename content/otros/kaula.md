+++
title = "Kaula"
+++
# Kaula
Archivos de "kaula", la plataforma esa del profe.

Todos estos archivos fueron descargados a mitad del 2019.

## Sistemas operativos y redes
Más info en [la página correspondiente](/informatica/sistemas-operativos-y-redes/)

- [01: La vida en un mundo centrado en la red](https://download.iru.cl/utal/kaula/32/1470167798tema1.pdf)
- [02: Comunicación a través de la red](https://download.iru.cl/utal/kaula/32/1470167832tema2.pdf)
- [03: Protocolos y Funcionalidad de la Capa de Aplicación](https://download.iru.cl/utal/kaula/32/1470167887tema3.pdf)
- [04: Capa de Transporte del modelo OSI](https://download.iru.cl/utal/kaula/32/1470167924tema4.pdf)
- [05: Protocolos y funciones de la capa de red](https://download.iru.cl/utal/kaula/32/1470167962tema5.pdf)
- [06: Direccionamiento IPv4](https://download.iru.cl/utal/kaula/32/1470168046tema6.pdf)
- [07: Capa de enlace de datos](https://download.iru.cl/utal/kaula/32/1470168079tema7.pdf)
- [08: Capa física del modelo OSI](https://download.iru.cl/utal/kaula/32/1470168107tema8.pdf)
- [09: Protocolo Ethernet](https://download.iru.cl/utal/kaula/32/1470168145tema9.pdf)

## Desconocido
- [01: Algoritmos, diagramas de flujo y programas](https://download.iru.cl/utal/kaula/35/1470231894tema1.pdf)
- [02: Algoritmos, diagramas de flujo y programas](https://download.iru.cl/utal/kaula/35/1470231912tema2.pdf)
- [03: Estructuras Algorítmicas Repetitivas](https://download.iru.cl/utal/kaula/35/1470231931tema3.pdf)
- [04: Estructuras de Datos: Arreglos](https://download.iru.cl/utal/kaula/35/1470234178tema4.pdf)
- [05: Planificación de CPU](https://download.iru.cl/utal/kaula/35/1470235474tema5-ssoo.pdf)
- [Introducción a la programación orientada a objetos](https://download.iru.cl/utal/kaula/54/1489696856tema1.pdf)
- [Más sobre POO](https://download.iru.cl/utal/kaula/54/1489699310tema2.pdf)

## Sistemas Operativos
Más info en [la página correspondiente](/informatica/sistemas-operativos-y-redes/)
- [Sistemas Operativos: Introducción](https://download.iru.cl/utal/kaula/53/1496256956tema1.pdf)
- [Estructuras de Sistemas Operativos](https://download.iru.cl/utal/kaula/53/1488398515tema2_ssoo.pdf)
- [Procesos](https://download.iru.cl/utal/kaula/53/1488398542tema3_ssoo.pdf)
- [Hebras](https://download.iru.cl/utal/kaula/53/1488398565tema4_ssoo.pdf)
- [Planificación de CPU](https://download.iru.cl/utal/kaula/53/1488398593tema5_ssoo.pdf)
- [Sincronización de Procesos](https://download.iru.cl/utal/kaula/53/1488398621tema6_ssoo.pdf)
- [Bloques Mutuos](https://download.iru.cl/utal/kaula/53/1488398652tema7_ssoo.pdf)
- [Memoria Principal](https://download.iru.cl/utal/kaula/53/1488398676tema8_ssoo.pdf)
- [Memoria Virtual](https://download.iru.cl/utal/kaula/53/1488398711tema9_ssoo.pdf)

## Taller de desarrollo web
Más info en [la página correspondiente](/informatica/taller-web/)
- [Tecnologías web](https://download.iru.cl/utal/kaula/55/1490017440tema1.pdf)
- [HTML y CSS](https://download.iru.cl/utal/kaula/55/1490023936tema2.pdf)
- [Apache web Server](https://download.iru.cl/utal/kaula/55/1490033802tema3.pdf)
- [SGBD PostgreSQL](https://download.iru.cl/utal/kaula/55/1490042520tema4.pdf)
- [PHP](https://download.iru.cl/utal/kaula/55/1490045468tema5.pdf)

## Estructuras de datos
Más info en [la página correspondiente](/informatica/algoritmos-y-estructura-de-datos/)
- [Introducción a las Estructuras de Datos](https://download.iru.cl/utal/kaula/57/1502908157tema1.pdf)
- [Pilas y Colas (Con arreglos)](https://download.iru.cl/utal/kaula/57/1502908222tema2.pdf)
- [Listas](https://download.iru.cl/utal/kaula/57/1502908275tema3.pdf)
- [Árboles: Árboles Binarios de Búsqueda (ABB)](https://download.iru.cl/utal/kaula/57/1540393730tema4.pdf)
- [Arboles: Aboles Balanceados (AVL) (sic)](https://download.iru.cl/utal/kaula/57/1502908384tema5.pdf)
- [Grafos](https://download.iru.cl/utal/kaula/57/1502908461tema6.pdf)
- [Análisis de Algoritmos](https://download.iru.cl/utal/kaula/57/1502908516tema7.pdf)
- [Métodos de Ordenamiento](https://download.iru.cl/utal/kaula/57/1502908553tema8.pdf)
- [Métodos de Búsqueda Interna](https://download.iru.cl/utal/kaula/57/1502908595tema9.pdf)

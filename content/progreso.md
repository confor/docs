+++
bookHidden = true
+++
# Progreso
un registro de todos los ramos de ingeniería civil en bioinformática y lo que hay disponible hasta el momento

los ramos de deporte, idiomas, civil y tesis **no** son considerados.

- 1 = guias
- 2 = pruebas
- 3 = materia (presentaciones)
- 4 = apuntes
- x = listo
- p = parcial

| ok | 1 | 2 | 3 | 4 | ramo |
|----|---|---|---|---|------|
|    | x |   |   |   | INTRODUCCION A LAS MATEMATICAS
|    | x |   |   |   | ALGEBRA
|    | x |   |   |   | QUIMICA GENERAL
|    | x |   |   | x | INTRODUCCION A LA INGENIERIA EN BIOINFORMATICA
|    | x |   |   |   | SOLUCIONES ALGORITMICAS
| no | - | - | - | - | COMUNICACION ORAL Y ESCRITA I
|    |   |   |   |   | CALCULO I
|    | x | p |   | p | ALGEBRA LINEAL
|    |   |   |   |   | QUIMICA ORGANICA
|    |   |   |   |   | ORGANIZACION ESTRUCTURAL DE LA CELULA
|    | x |   |   |   | PROGRAMACION I
| no | - | - | - | - | COMUNICACION ORAL Y ESCRITA II
|    |   |   |   |   | CALCULO II
|    | x | x | x |   | FISICA GENERAL
|    |   | x |   |   | PROCESOS METABOLICOS CELULARES
|    | x | x | x |   | PROBABILIDAD Y ESTADISTICA
|    |   |   |   |   | PROGRAMACION AVANZADA
| no | - | - | - | - | AUTOGESTION DEL APRENDIZAJE
|    |   |   |   |   | ECUACIONES DIFERENCIALES
|    |   |   |   |   | ELECTRICIDAD Y MAGNETISMO
|    |   |   |   |   | EXPRESION GENICA Y SU REGULACION
|    | x |   | x |   | ALGORITMOS Y ESTRUCTURA DE DATOS
| no | - | - | - | - | IDIOMA EXTRANJERO I
|    |   |   |   |   | TRABAJO EN EQUIPO Y DESARROLLO DE HABILIDADES SOCIALES
|    |   |   |   |   | TERMODINAMICA
|    | x | x |   | x | ANALISIS DE SECUENCIAS BIOLOGICAS
|    |   |   |   |   | ORGANIZACION Y DINAMICA DEL GENOMA
|    | x | x | x | x | SISTEMAS OPERATIVOS Y REDES
| no | - | - | - | - | IDIOMA EXTRANJERO II
|    |   |   |   |   | COMPRENSION DE CONTEXTOS SOCIALES
|    |   |   |   |   | TALLER DE INTEGRACION
|    |   |   |   |   | BIOFISICA
|    |   |   |   |   | TEORIA DE SISTEMAS
|    |   |   | x |   | BASES DE DATOS
| no | - | - | - | - | IDIOMA EXTRANJERO III
|    |   |   |   |   | COMPRENSION DE CONTEXTOS CULTURALES
| no | - | - | - | - | DEPORTE I
|    |   |   |   |   | MODELOS MATEMATICOS EN SISTEMAS BIOLOGICOS
|    |   |   |   |   | BIOINFORMATICA ESTRUCTURAL
|    |   |   |   |   | MINERIA DE DATOS
|    |   |   | x |   | TALLER DE PROGRAMACION WEB
| no | - | - | - | - | IDIOMA EXTRANJERO IV
| no | - | - | - | - | ETICA Y RESPONSABILIDAD SOCIAL
|    |   |   |   |   | ENSAMBLADO Y ANOTACION DE GENOMAS
|    |   |   |   |   | SIMULACION MOLECULAR I
|    |   |   |   |   | FUNDAMENTOS DE ADMINISTRACION
|    |   |   |   |   | PROCESOS BIOINDUSTRIALES
| no | - | - | - | - | IDIOMA EXTRANJERO V
| no | - | - | - | - | RESPONSABILIDAD SOCIAL
| no | - | - | - | - | DEPORTE II
| no | - | - | - | - | ELECTIVO I
| no | - | - | - | - | ELECTIVO II
|    |   |   |   |   | BIOTECNOLOGIA
|    |   |   |   |   | INGENIERIA ECONOMICA Y EVALUACION DE PROYECTOS
|    |   |   |   |   | GESTION DE RECURSOS HUMANOS
|    |   |   |   |   | IDIOMA EXTRANJERO VI
| no | - | - | - | - | ELECTIVO III
| no | - | - | - | - | PROYECTO DE MEMORIA DE TITULO
|    |   |   |   |   | TALLER DE PROYECTOS BIOTECNOLOGICOS
|    |   |   |   |   | GESTION DE LA INNOVACION Y EMPRENDIMIENTO
| no | - | - | - | - | ELECTIVO IV
| no | - | - | - | - | MEMORIA DE TITULO
| no | - | - | - | - | MODULO INTEGRADO DE COMPETENCIAS I
| no | - | - | - | - | MODULO INTEGRADO DE COMPETENCIAS II

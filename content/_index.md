+++
title = "Inicio"
weight = 1
layout = "single"
+++
# Sobre el repositorio
Una lista de documentos e información ordenada para ayudar con la vida universitaria. Hay apuntes, guias, pruebas, materia, libros, guias y código.

**Comparte esta información** con tus compañeros, sobre todo si te sirve.

Revisa la página [aportes](aportes) si quieres contribuir o ayudar.

## Disclaimer
Todos los archivos vienen de los mismos alumnos y tengo su permiso para compartir y divulgar lo que me entregan. Las presentaciones son propiedad intelectual de los profesores, supongo.

## Contenido
Contenido disponible de los ramos de la carrera; lentamente van apareciendo más archivos y apuntes. Asegúrate de [compartir lo que tengas](aportes)!

#### Nota
Los ramos marcados con un * asterisco tienen contenido que no está público todavía. Lo puedes pedir por WhatsApp.

#### Ramos por semestre
- Semestre 1
    - [Introducción a las matemáticas](/ramos/introduccion-a-las-matematicas/)
    - [Álgebra](/ramos/algebra/)
    - [Química general](/ramos/quimica-general/)
    - [Introducción a la ingeniería en bioinformática](/ramos/introduccion-a-la-ingenieria-en-bioinformatica/)
    - [Soluciones algorítmicas](/ramos/soluciones-algoritmicas/)
    - [Comunicación oral y escrita I](/ramos/comunicacion-oral-y-escrita-i/)
- Semestre 2
    - [Cálculo I](/ramos/calculo-i/)
    - [Álgebra lineal](/ramos/algebra-lineal/)
    - [Química orgánica](/ramos/quimica-organica/)
    - [Organización estructural de la célula](/ramos/organizacion-estructural-de-la-celula/)
    - [Programación I](/ramos/programacion-i/)
    - [Comunicación oral y escrita II](/ramos/comunicacion-oral-y-escrita-ii/)
- Semestre 3
    - [Cálculo II](/ramos/calculo-ii/)
    - [Física general](/ramos/fisica-general/)
    - [Procesos metabólicos celulares](/ramos/procesos-metabolicos-celulares/)
    - [Probablilidad y estadística](/ramos/probablilidad-y-estadistica/)
    - [Programación avanzada](/ramos/programacion-avanzada/)
    - [Autogestión del aprendizaje](/ramos/autogestion-del-aprendizaje/)
- Semestre 4
    - [Ecuaciones diferenciales](/ramos/ecuaciones-diferenciales/)
    - [Electricidad y magnetismo](/ramos/electricidad-y-magnetismo/)
    - [Expresión génica y su regulación](/ramos/expresion-genica-y-su-regulacion/)
    - [Algoritmos y estructura de datos](/ramos/algoritmos-y-estructura-de-datos/)
    - Idioma extranjero I
    - [Trabajo en equipo y desarrollo de habilidades sociales](/ramos/trabajo-en-equipo-y-desarrollo-de-habilidades-sociales/)
- Semestre 5
    - [Termodinámica](/ramos/termodinamica/)
    - [Análisis de secuencias biológicas](/ramos/analisis-de-secuencias-biologicas/)
    - [Organización y dinámica del genoma](/ramos/organizacion-y-dinamica-del-genoma/)
    - [Sistemas operativos y redes](/ramos/sistemas-operativos-y-redes/)
    - Idioma extranjero II
    - [Comprensión de contextos sociales](/ramos/comprension-de-contextos-sociales/)
- Semestre 6
    - [Taller de integración](/ramos/taller-de-integracion/)
    - [Biofísica](/ramos/biofisica/)
    - [Teoría de sistemas](/ramos/teoria-de-sistemas/)
    - [Base de datos](/ramos/base-de-datos/)
    - Idioma extranjero III
    - [Comprensión de contextos culturales](/ramos/comprension-de-contextos-culturales/)
    - [Deporte I](/ramos/deporte-i/)
- Semestre 7
    - [Modelos matemáticos en sistemas biológicos](/ramos/modelos-matematicos-en-sistemas-biologicos/)
    - [Bioinformática estructural](/ramos/bioinformatica-estructural/)
    - [Minería de datos](/ramos/mineria-de-datos/)
    - [Taller de programación web](/ramos/taller-de-programacion-web/)
    - Idioma extranjero IV
    - [Ética y responsabilidad social](/ramos/etica-y-responsabilidad-social/)
- Semestre 8
    - [Ensamblado y anotación de genomas](/ramos/ensamblado-y-anotacion-de-genomas/)
    - [Simulación molecular I](/ramos/simulacion-molecular-i/)
    - [Fundamentos de administración](/ramos/fundamentos-de-administracion/)
    - [Procesos bioindustriales](/ramos/procesos-bioindustriales/)
    - Idioma extranjero V
    - [Responsabilidad social](/ramos/responsabilidad-social/)
    - [Deporte II](/ramos/deporte-ii/)
- Semestre 9
    - [Electivo I](/ramos/electivo-i/)
    - [Electivo II](/ramos/electivo-ii/)
    - [Biotecnología](/ramos/biotecnologia/)
    - [Ingeniería económica y evaluación de proyectos](/ramos/ingenieria-economica-y-evaluacion-de-proyectos/)
    - Idioma extranjero VI
    - [Gestión de recursos humanos](/ramos/gestion-de-recursos-humanos/)
- Semestre 10
    - [Electivo III](/ramos/electivo-iii/)
    - [Proyecto de memoria de título](/ramos/proyecto-de-memoria-de-titulo/)
    - [Taller de proyectos biotecnológicos](/ramos/taller-de-proyectos-biotecnologicos/)
    - [Gestión de la innovación y emprendimiento](/ramos/gestion-de-la-innovacion-y-emprendimiento/)
- Semestre 11
    - [Electivo IV](/ramos/electivo-iv/)
    - [Memoria de título](/ramos/memoria-de-titulo/)

+++
title = "Horarios"
bookToC = true
+++
# Horarios 2020/1
Los horarios para el primer semestre de bioinformática del 2020.

{{< hint danger >}}
**Ya han habido modificaciones y puede que hayan más**. Consulta tu utalmático para información oficial.
{{< /hint >}}

Última actualización: 9 de abril

## Álgebra lineal
Sección A
- jue 08:30 - 09:30 cátedra
- jue 09:40 - 10:40 lab
- mar 08:30 - 10:40 cátedra
- mie 17:50 - 20:00 ayudantia

Sección B
- jue 08:30 - 09:30 cátedra
- jue 09:40 - 10:40 lab
- mar 08:30 - 10:40 cátedra
- mie 17:50 - 20:00 ayudantia

## Álgebra
Sección A
- jue 08:30 - 09:30 cátedra
- jue 09:40 - 10:40 lab
- mar 08:30 - 10:40 cátedra
- mie 17:50 - 20:00 ayudantia

Sección B
- jue 08:30 - 09:30 cátedra
- jue 09:40 - 10:40 lab
- mar 08:30 - 10:40 cátedra
- mie 17:50 - 20:00 ayudantia

## Análisis de secuencias biológicas
Sección A
- mar 08:30 - 11:50 cátedra
- mar 16:40 - 17:40 lab
- mar 17:50 - 18:50 ayudantia

## Autogestión del aprendizaje
Sección A
- jue 08:30 - 11:50 cátedra

Sección B
- jue 08:30 - 11:50 cátedra

Sección C
- jue 08:30 - 11:50 cátedra

Sección D
- jue 08:30 - 11:50 cátedra

## Biofísica
Sección A
- mie 08:30 - 09:30 lab
- mie 09:40 - 11:50 cátedra

## Bioinformática estructural
Sección A
- jue 09:40 - 11:50 lab
- mie 10:50 - 13:00 cátedra
- mie 19:00 - 20:00 ayudantia

## Biotecnología
Sección A
- lun 08:30 - 10:40 cátedra
- mar 08:30 - 09:30 cátedra
- mar 09:40 - 10:40 lab

## Cálculo I
Sección A
- jue 17:50 - 18:50 lab
- jue 19:00 - 20:00 ayudantia
- lun 10:50 - 13:00 cátedra
- mar 10:50 - 11:50 cátedra
- mar 12:00 - 13:00 taller

## Cálculo II
Sección A
- jue 17:50 - 20:00 ayudantia
- mie 10:50 - 13:00 cátedra
- vie 10:50 - 11:50 cátedra
- vie 12:00 - 13:00 lab

## COE I
Sección A
- mar 14:20 - 16:30 cátedra

Sección B
- mar 14:20 - 16:30 cátedra

## Ética y responsabilidad social
Sección A, B, C
- mar 09:40 - 11:50 cátedra
- mar 12:00 - 13:00 terreno

## Física general
Sección A
- jue 16:40 - 17:40 ayudantia
- lun 15:30 - 17:40 cátedra
- mar 10:50 - 11:50 cátedra
- mar 15:30 - 16:30 cátedra
- mar 16:40 - 17:40 lab

## Genómica funcional y metagenómica (minor)
Sección A
- lun 14:20 - 16:30
- mie 14:20 - 16:30

## Gestión de recursos  humanos
Sección A
- vie 15:30 - 17:40 cátedra

## Ingeniería económica y evaluación de proyectos
Sección A
- lun 10:50 - 13:00 cátedra
- lun 19:00 - 20:00 ayudantia
- mie 08:30 - 10:40 cátedra

## Introducción a la ingeniería en bioinformática
Sección A (lo siento por quien tome este ramo)
- lun 15:30 - 18:50 cátedra

## Introducción a las matemáticas
Sección A
- jue 17:50 - 18:50 ayudantia
- lun 10:50 - 13:00 cátedra
- mar 10:50 - 11:50 cátedra
- mar 12:00 - 13:00 lab

Sección B
- jue 17:50 - 18:50 ayudantia
- lun 10:50 - 13:00 cátedra
- mar 10:50 - 11:50 cátedra
- mar 12:00 - 13:00 lab

## Memoria de título
Sección A
- vie 10:50 - 13:00 cátedra

## Minería de datos
Sección A
- mie 15:30 - 17:40 cátedra
- mie 17:50 - 18:50 ayudantia
- vie 08:30 - 09:30 cátedra
- vie 09:40 - 11:50 lab

## Modelos matemáticos en sistemas biológicos
Sección A
- jue 17:50 - 18:50 lab
- jue 19:00 - 20:00 ayudantia
- mie 08:30 - 10:40 cátedra

## Probabilidad y estadística
Sección A
- mie 14:20 - 16:30 cátedra
- mar 17:50 - 20:00 lab
- vie 08:30 - 10:40 cátedra

## Procesos metabólicos celulares
Sección A
- mar 08:30 - 10:40 cátedra
- mie 16:40 - 17:40 cátedra
- mie 17:50 - 20:00 lab

## Programación avanzada
Sección A
- vie 15:30 - 17:40 cátedra
- vie 17:50 - 21:10 lab

## Proyecto de memoria de título
Sección A
- vie 10:50 - 13:00 cátedra

## Química general
Sección A
- lun 08:30 - 10:40 cátedra
- mie 10:50 - 13:00 lab
- mie 13:10 - 14:10 ayudantia

## Sistemas operativos y redes
Sección A
- jue 15:30 - 17:40 lab
- lun 08:30 - 11:50 cátedra

## Soluciones algorítmicas
Sección A
- vie 08:30 - 10:40 cátedra
- vie 10:50 - 14:10 lab

## Taller de programación web
Sección A
- lun 15:30 - 20:00 lab
- mar 15:30 - 17:40 cátedra

## Termodinámica
Sección A
- jue 08:30 - 10:40 lab
- lun 15:30 - 18:50 cátedra

## Cambios
- agregado `Genómica funcional y metagenómica (minor)`. fuente: captura de pantalla del utalmático de alguien en whatsapp
- cambiado el lab de `Probabilidad y estadística` del miércoles al martes. esto soluciona un tope de horario con `Procesos metabólicos celulares`. fuente: correo del director de icb.

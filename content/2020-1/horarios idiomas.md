+++
title = "Horarios idiomas"
BookToC = true
+++
# Horarios idiomas 2020/1
Los horarios del programa de idiomas, primer semestre del 2020.

{{< hint info >}}
Si tienes los horarios del resto de las secciones, compártelos!
{{< /hint >}}

## Inglés
### A2-I
Sección A
- jue 17:50 - 20:00

Sección B
- vie 13:10 - 15:20

Sección C
- mar 17:50 - 18:50
- mie 17:50 - 18:50

Sección D
- mar 17:50 - 18:50
- mie 17:50 - 18:50

Sección E
- lun 12:00 - 13:00
- mie 09:40 - 10:40

Sección F
- lun 10:50 - 11:50
- mie 08:30 - 09:30

Sección G
- lun 14:20 - 15:20
- mar 14:20 - 15:20

### A2-II (grupo 1)
Sección A
- mar 10:50 - 11:50
- mie 10:50 - 11:50

Sección B
- jue 09:40 - 10:40
- vie 09:40 - 10:40

Sección C
- lun 14:20 - 15:20
- mie 10:50 - 11:50

Sección D
- mar 15:30 - 16:30
- vie 12:00 - 13:00

Sección E
- vie 08:30 - 10:40

Sección F
- vie 08:30 - 10:40

Sección G
- mar 17:50 - 18:50
- mie 12:00 - 13:00

Sección H
- lun 17:50 - 18:50
- mar 17:50 - 18:50

Sección I
- lun 12:00 - 13:00
- vie 08:30 - 09:30

Sección J
- mar 09:40 - 10:40
- mie 08:30 - 09:30

Sección K
- mar 10:50 - 11:50
- vie 13:10 - 14:10

Sección L
- lun 17:50 - 18:50
- jue 17:50 - 18:50

Sección M
- lun 17:50 - 18:50
- vie 14:20 - 15:20

Sección N
- vie 15:30 - 17:40

Sección O
- mar 16:40 - 17:40
- mie 13:10 - 14:10

Sección P
- mar 19:00 - 20:00
- jue 17:50 - 18:50

Sección Q
- mar 19:00 - 20:00
- jue 17:50 - 18:50

Sección R
- lun 15:30 - 16:30
- jue 15:30 - 16:30

Sección S
- lun 12:00 - 13:00
- mar 09:40 - 10:40

Sección T
- mar 08:30 - 09:30
- jue 17:50 - 18:50

Sección U
- lun 17:50 - 18:50
- vie 17:50 - 18:50

Sección V
- lun 12:00 - 13:00
- mie 16:40 - 17:40

Sección W
- lun 14:20 - 15:20
- mar 14:20 - 15:20

Sección X
- mie 10:50 - 11:50
- jue 08:30 - 09:30

Sección Y
- mie 10:50 - 11:50
- jue 08:30 - 09:30

Kinesiología (3424B218)
- mie 16:40 - 17:40
- jue 09:40 - 10:40

Medicina (3420B219)
- jue 14:20 - 16:30
- jue 15:30 - 17:40

### B1-I
Sección A
- lun 17:50 - 18:50
- jue 17:50 - 18:50

Sección B
- lun 14:20 - 15:20
- vie 14:20 - 15:20

Sección C
- mie 17:50 - 18:50
- vie 10:50 - 11:50

Sección D
- mar 19:00 - 20:00
- mie 19:00 - 20:00

Sección E
- mie 14:20 - 16:30

Sección F
- mie 16:40 - 17:40
- jue 16:40 - 17:40

Sección G
- mie 17:50 - 18:50
- jue 17:50 - 18:50

Sección H
- lun 19:00 - 20:00
- jue 19:00 - 20:00

Sección I
- lun 19:00 - 20:00
- vie 12:00 - 13:00

### B1-II
Faltan secciones de A a P

### B1-III
Faltan secciones de A a I

### B2-I
Sección A
- mar 08:30 - 09:30
- jue 16:40 - 17:40

Sección B
- mar 14:20 - 15:20
- vie 15:30 - 16:30

### B2-II
Sección A
- lun 14:20 - 15:20
- vie 15:30 - 16:30

Sección B
- mar 19:00 - 20:00
- mie 19:00 - 20:00

### B2-III
Sección A
- mie 14:20 - 15:20
- jue 08:30 - 09:30

Sección B
- lun 17:50 - 20:00

Sección C
- jue 19:00 - 20:00
- vie 16:40 - 17:40

### C1-I
- lun 13:10 - 15:20

### C1-II
- vie 13:10 - 15:20

### C1-III
- jue 16:40 - 18:50

## Francés
### A1
- lun 12:00 - 14:10
- mie 12:00 - 14:10

## Alemán
### A1
Sección A
- lun 13:10 - 15:20
- mie 13:10 - 15:20

Sección B
- mar 15:30 - 17:40
- jue 15:30 - 17:40

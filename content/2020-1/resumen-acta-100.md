+++
title = "Resumen cae acta 100"
bookToC = false
bookHidden = true
+++
## resumen cae acta 100
esta página resume el acta 100 del consejo académico extraordinario.

{{< hint danger >}}
**esto es un resumen brusco de la respuesta al ultimo petitorio.** es una apreciación de un alumno. consulta con tu centro de alumnos para leer el documento completo.
{{< /hint >}}

el 01/jul y 02/jul se junto el rector con sendo montón de directores. participaron dos alumnos. vieron el petitorio de los estudiantes movilizados de la utalca talca.

### resoluciones

#### re-programación de evaluaciones
- no habrá

#### recalendarización
- el semestre cierra el 21-26 de agosto
- el 2do semestre empieza el 02 de septiembre

#### marcha blanca
- habrán 4 dias

#### clases
- no se van a repetir
- se hará una clase resumen
- los profes deben dejar grabaciones/material online
- el material debe estar al menos 48 horas antes
- el syllabus abandonará la bibliografia complementaria
- las clases serán bloques de 50 min y 20 min de recreo
- la directiva de escuela hará llegar una lista de horarios de atención de los profes

#### evaluaciones
- si se modifican las ponderaciones todos deben ser avisados
- las pruebas por educandus serán "de carácter libre no sucesivo"
- los profes pueden hacer cambios para evitar copiar
- los directores deberían revisar la carga académica
- hay un protocolo nuevo de evaluaciones a distancia
- el contenido de las pruebas debe estar en el syllabus
- las cámaras "dependerán del aprendizaje a desarrollar o evaluar"
- habrá flexibilidad en caso de contagio
- cada facultad ve cómo lidia con eximir de exámenes finales
- sólo hay una recuperativa
- la recuperativa ahora se podrá dar igual para subir una nota

#### reglamento del régimen de estudios
- el art. 21 se mantiene

#### becas y ayudas
- "se sugiere que los alumnos soliciten ayudas evantuales cuando lo requieran"

#### carga académica
- el director de cada escuela es el encargado de ver la carga académica
- los alumnos pueden acusar faltas hacia el centro de alumnos

#### postergación de estudios
- habrá flexibilidad (de parte del ministerio?)
- la vicerrectoría avisará sobre los beneficios que se perderán por una carta
- el rector solicitará al ministerio más plazo para postergar estudios

#### padres y madres
- se debería avisar al director de la escuela respectiva si uno es papá/mamá

#### aranceles y matrículas
- no se cambiarán los aranceles
- las devoluciones de pago de matrícula (gratuidad) están funcionando normal
- las matrículas suben de acuerdo al IPC
- se podrán patear 5 cuotas del 2020 hacia el futuro
- durante el 2020 no habrán multas por atraso (sólo pregrado)
- pagos por transbank/cheque tendrán un trato especial?

#### equipos y conectividad
- a inicio de 2do semestre se entregarán notebooks a quienes tengan 120 créditos
- se debe avisar al director de escuela si uno tiene problemas de internet
- RECIÉN EL 30/JUN LA DTI COMENZARÁ LA ATENCIÓN POR PARTE DE LA MESA DE AYUDA
- la u compró licencias microsoft teams
- se les enseñó a los profes a usar el computador
- los directores de escuela podrán gestionar prestamos de computadores (si es que hay)

#### salud mental
- habrá apoyo médico y psicológico: "se ha aumentado la atención psicológica"
- los psicólogos tienen que ser parte de "la dirección de salud" y no puede ser cualquier psicólogo
- estudiantes atendiendo estudiantes viola algo ético, creo

#### covid
- ya hay protocolos de salud (= no ir a la u) del gobierno

#### recuperación de alumno regular
- se puede rogar al decano

#### otros
- no habrán sanciones por participar en el paro
- se verá si es factible hacer algunos ramos durante el verano
- se harán talleres no-obligatorios a los profes para "sus habilidades sociales"
- cualquier duda con el art. 35 se tiene que llevar a la vicerrectoría de pregrado

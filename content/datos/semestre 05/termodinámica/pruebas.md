+++
title = "Termodinámica"
url = "/ramos/termodinamica/pruebas/"
+++
Artículo principal en [termodinámica](../).

## Fuente
Ing. civil en Bioinformática, 2017 / 1.

{{< hint info >}}
Estas pruebas vienen de un scan del 2019 de una prueba del 2017. Los errores son de la redacción del profe!
{{< /hint >}}

## Prueba 1 (2017/1)
Pregunta 1
: En un recipiente aislado térmicamente se mezclan 68473 g de un concentrado de tomato, a 70 °C, con 500 kg de agua a 30 °C, para poder iniciar la fabricación de una salsa. ¿Cuál será la temperatura de la mezcla? (cp conc tomate = 2,83 kJ/kg.K; cp agua = 4,18 kJ/kg.K). Se debe indicar claramente los supuestos por usted utilizados para elaborar su respuesta.

Pregunta 2
: 5 kilogramos de agua a 20 °C están contenidos en un cilindro verticalmente equipado con un pistón sin fricción con una masa tal que la presión del agua es de 800 kpa. Se transfiere calor lentamente al agua, provocando que el émbolo suba hasta los topes en el cual el volumen interior del cilindro es de 0.7 m^3. Se continúa el suministro de calor al agua hasta que esta se convierte en vapor saturado. Responda lo siguiente:
  - Encuentre la presión final del cilindro, al final del proceso.
  - Describa las distintas fases por las que atraviesa el cilindro en este proceso.
  - Represente el proceso descrito en los diagramas de tipo P-v y T-v

Pregunta 3
: Considere el siguiente proceso en 2 etapas.
  1. 10 kg de refrigerante 134a, a una presión de 300 kPa, llenan un recipiente rigido cuyo volumen es de 14 L. Se pide determinar en principio (a) la temperatura y (b) la entalpía total en el recipiente en este estado.
  2. Luego de esto el recipiente se calienta hasta que la presión es de 600 kPa. Determinar nuevamente en esta caso (c) la temperatura y (d) la entalpía total del refrigerante, cuando el calentamiento se termina.

Pregunta 4
: Complete la siguiente tabla de propiedades del agua (H2O) utilizando la información entregada para caso. En la última columna, describa la condición del agua en alguna de estas fases: líquido comprimido,, mezcla saturada, vapor sobrecalentado o información insuficiente; y si es aplicable, indique la calidad de la mezcla (x) en la columna anterior.

| T-°C | P - kPa | H - kJ/kg |  X  | Descripción de fase |
|------|---------|-----------|-----|---------------------|
|  -   | 200     |    -      | 0,7 |                     |
| 140  |         |  1800     |     |                     |
|      | 950     |           | 0,0 |                     |
| 80   | 5000    |           |     |                     |

5. Un dispositivo de cilindro-émbolo contiene inicialmente 1.4 kg de agua líquda saturada a 200 °C. Entonces, se transmite calor al agua en el recipiente, hasta que se cuadriplica el volumen. En este escenario, el sistema llega al punto en donde sólo se encuentre vapor saturado. Se pide determinar lo siguiente:
   - el volumen del recipiente al inicio y al final del proceso.
   - la temperatura y presión finales que alcanza el sistema.
   - el cambio de energía interna del agua (ΔU)

## Prueba 2 (2017/1)
Pregunta 1
: Un dispositivo aislado de cilindo-émbolo contiene 5 L de agua liquida saturada a una presión constante de 175 kPa. Una rueda de paletas agita el agua, mientras que pasa una corriente de 8 A durante 45 min, por una resistencia colocada en el agua. Si se evapora la mitad del liquido durante este proceso a presión constante, y el trabajo de la rueda de paletas es 400 kJ, se pide:
  - Determine el voltaje de suministro asociado a la resistencia eléctrica
  - Muestre el proceso asociado al sistema en un diagrama P-V con respecto a líneas de saturación.

Pregunta 2
: Considere la siguiente situación. A un tubo aislado entra vapor de agua a 200 kPa y 200 °C, y sale a 150 kPa y 150 °C.
: Si la relación de diámetros de entrada entre salida para ese tubo es D1/D2 = 1.80. Se pide determina las velocidades de entrada y salida del vapor al sistema.

Pregunta 3
: Considere un sistema en donde se mezclan las corrientes caliente y fria de un fluido en una cámara de mezclaro rígida.
: El fluido caliente fluye a la cámara a un flujo másico de 5 kg/s, con una cantidad de energía de 150 kJ/kg.
: El fluido frío fluye a la cámara a un flujo másico de 15 kg/s y lleva una cantidad de energía de 50 kJ/kg.
: Considere que hay transferencia de calor al entorno de la cámara de mezclado, en la cantidad de 5.5 kW. La cámara de mezclado opera con flujo estacionario y no gana ni pierde energía ni masa con el tiempo.
: Se pide determinar la energía transportada por la corriente de salida de la cámara de mezclado por unidad de masa de fluido.

Pregunta 4
: Se usarán gases caliente de escape de un monitor de combustión interna, para producir vapor saturado a 2 MPa.
: Los gases de escape entran al intercambiador de calor a 400°C, con un flujo de 32 kg/min, mientras que el agua entra a 15 °C.
: El intercambiador de calor no está bien aislado, y se estima que el 10 por ciento del calor cedido por los gases de escape se pierde a los alrededores.
: Si el flujo másico de gases de escape es 15 veces el del agua, determine:
  - la temperatura d los gases de escape en la salida del intercambiador de calor
  - la tasa de transferencia del calor al agua

(falta un diagrama aquí)

Pregunta 5
: Una turbina de vapor trabaja con vapor de agua a 1.6 MPa y 350 °C en su entrada, y vapor saturado a 30 °C en su salida. El flujo másico del vapor es de 16 kg/s y la turbina produce 9000 kW de potencia. Determina la rapidez con que se pierde calor a través de la carcasa de esa turbina (medido en kW).


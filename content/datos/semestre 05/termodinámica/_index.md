+++
title = "Termodinámica"
url = "/ramos/termodinamica/"
+++

- Unidad I: Reconocer las leyes de la termodinámica y sus relaciones matemáticas
- Unidad II: Relacionar los parámetros físicos en las transformaciones químicas con las leyes termodinámicas.
- Unidad III: Concluir resultados acordes con las propiedades termodinámicas obtenidas a partir de diferentes metodologías computacionales.

Hay cátedras, guias con ejercicios, tres pruebas convencionales (lapiz y papel) y un seminario final.

## Fuente
Ing. civil en Bioinformática, 2017 / 1.

## Evaluaciones
Más información en [pruebas](pruebas/).

## Archivos
Algunas pruebas están en la página [pruebas](pruebas/).

## Progreso
{{< progreso fuente="2017/1" syllabus="no" pruebas="si" guias="no" desc="si" >}}

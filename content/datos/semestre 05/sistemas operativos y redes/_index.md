+++
title = "Sistemas operativos y redes"
url = "/ramos/sistemas-operativos-y-redes/"
+++

## Fuente
"Kaula", más info [aquí](/otros/kaula/).

## Archivos
- [01: La vida en un mundo centrado en la red](https://download.iru.cl/utal/kaula/32/1470167798tema1.pdf)
- [02: Comunicación a través de la red](https://download.iru.cl/utal/kaula/32/1470167832tema2.pdf)
- [03: Protocolos y Funcionalidad de la Capa de Aplicación](https://download.iru.cl/utal/kaula/32/1470167887tema3.pdf)
- [04: Capa de Transporte del modelo OSI](https://download.iru.cl/utal/kaula/32/1470167924tema4.pdf)
- [05: Protocolos y funciones de la capa de red](https://download.iru.cl/utal/kaula/32/1470167962tema5.pdf)
- [06: Direccionamiento IPv4](https://download.iru.cl/utal/kaula/32/1470168046tema6.pdf)
- [07: Capa de enlace de datos](https://download.iru.cl/utal/kaula/32/1470168079tema7.pdf)
- [08: Capa física del modelo OSI](https://download.iru.cl/utal/kaula/32/1470168107tema8.pdf)
- [09: Protocolo Ethernet](https://download.iru.cl/utal/kaula/32/1470168145tema9.pdf)
- [Sistemas Operativos: Introducción](https://download.iru.cl/utal/kaula/53/1496256956tema1.pdf)
- [Estructuras de Sistemas Operativos](https://download.iru.cl/utal/kaula/53/1488398515tema2_ssoo.pdf)
- [Procesos](https://download.iru.cl/utal/kaula/53/1488398542tema3_ssoo.pdf)
- [Hebras](https://download.iru.cl/utal/kaula/53/1488398565tema4_ssoo.pdf)
- [Planificación de CPU](https://download.iru.cl/utal/kaula/53/1488398593tema5_ssoo.pdf)
- [Sincronización de Procesos](https://download.iru.cl/utal/kaula/53/1488398621tema6_ssoo.pdf)
- [Bloques Mutuos](https://download.iru.cl/utal/kaula/53/1488398652tema7_ssoo.pdf)
- [Memoria Principal](https://download.iru.cl/utal/kaula/53/1488398676tema8_ssoo.pdf)
- [Memoria Virtual](https://download.iru.cl/utal/kaula/53/1488398711tema9_ssoo.pdf)

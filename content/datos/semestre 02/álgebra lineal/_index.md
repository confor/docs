+++
title = "Álgebra lineal"
url = "/ramos/algebra-lineal/"
+++

## Contenidos
- Unidad 1: Álgebra de matrices y sistemas de ecuaciones lineales
- Unidad 2: Espacios vectoriales de dimensión finita
- Unidad 3: Transformaciones lineales y diagonalización

El ramo usó "Introducción al álgebra lineal" de Larson (ed Limusa).

## Fuente
Ing. civil en Bioinformática, 2018/2.

## Evaluaciones
El ramo tuvo evaluaciones convencionales (lapiz y papel) de las tres unidades, controles web (a través de educandus), talleres (guias evaluadas en ayudantía) y un exámen final.

- [Prueba 1](http://download.iru.cl/utal/algebra-lineal-2018-2/certamen1.pdf)
- [Prueba 2](http://download.iru.cl/utal/algebra-lineal-2018-2/certamen2.pdf)
- [Prueba 3 (ejercicio b)](http://download.iru.cl/utal/algebra-lineal-2018-2/certamen3b.pdf)

## Archivos
- [Syllabus](http://download.iru.cl/utal/algebra-lineal-2018-2/syllabus.pdf)
- [Apuntes de transformación lineal + desarrollo control web 5](http://download.iru.cl/utal/algebra-lineal-2018-2/t_lin.pdf)
- [Guia 1](http://download.iru.cl/utal/algebra-lineal-2018-2/guia1.pdf)
- [Guia 2](http://download.iru.cl/utal/algebra-lineal-2018-2/guia2.pdf)
- [Guia 3](http://download.iru.cl/utal/algebra-lineal-2018-2/guia3.pdf)
- [Guia 4](http://download.iru.cl/utal/algebra-lineal-2018-2/guia4.pdf)
- [Guia 5](http://download.iru.cl/utal/algebra-lineal-2018-2/guia5.pdf)
- [Guia 6](http://download.iru.cl/utal/algebra-lineal-2018-2/guia6.pdf)
- [Guia 7](http://download.iru.cl/utal/algebra-lineal-2018-2/guia7.pdf)
- [Taller nov 19](http://download.iru.cl/utal/algebra-lineal-2018-2/taller%20nov%2019.pdf)
- [Taller dic 3](http://download.iru.cl/utal/algebra-lineal-2018-2/taller%20dic%203.pdf)
- [Taller dic 4](http://download.iru.cl/utal/algebra-lineal-2018-2/taller%20dic%204.pdf)
- [Taller dic 5](http://download.iru.cl/utal/algebra-lineal-2018-2/taller%20dic%205.pdf)

## Progreso
{{< progreso fuente="2018/2" syllabus="si" pruebas="si" guias="si" desc="si" >}}

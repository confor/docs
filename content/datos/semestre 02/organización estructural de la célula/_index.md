+++
title = "Organización estructural de la célula"
url = "/ramos/organizacion-estructural-de-la-celula/"
+++

## Fuente
Ing. civil en Bioinfoprmática, **2018 / 2**.

## Archivos
- [Syllabus 2018](https://download.iru.cl/utal/org-est-2018-2/Syllabus%202018.pdf)
- [Cronograma Catedra 2018 (contenido del curso)](https://download.iru.cl/utal/org-est-2018-2/Cronograma%20Catedra%202018.pdf)
- [Cronograma Laboratorio 2018 (contenido del laboratorio)](https://download.iru.cl/utal/org-est-2018-2/Cronograma%20Laboratorio%202018.pdf)
- [Cronograma Talleres 2018](https://download.iru.cl/utal/org-est-2018-2/Cronograma%20Talleres%202018.pdf)
- [Cronograma Trabajos Autonomos 2018](https://download.iru.cl/utal/org-est-2018-2/Cronograma%20Trabajos%20Autonomos%202018.pdf)
- [Guía para informes](https://download.iru.cl/utal/org-est-2018-2/guia%20informes.pdf)
- [Guía de laboratorio](https://download.iru.cl/utal/org-est-2018-2/guia%20laboratorio.pdf)

### Tareas
- [Taller 01](https://download.iru.cl/utal/org-est-2018-2/taller%2001.pdf)
- [Taller 02](https://download.iru.cl/utal/org-est-2018-2/taller%2002.pdf)
- [Taller 03](https://download.iru.cl/utal/org-est-2018-2/taller%2003.pdf)
- [Taller 04](https://download.iru.cl/utal/org-est-2018-2/taller%2004.pdf)
- [Taller 05](https://download.iru.cl/utal/org-est-2018-2/taller%2005.pdf)
- [Taller 06](https://download.iru.cl/utal/org-est-2018-2/taller%2006.pdf)
- [Taller 07](https://download.iru.cl/utal/org-est-2018-2/taller%2007.pdf)
- [Taller 08](https://download.iru.cl/utal/org-est-2018-2/taller%2008.pdf)
- [Taller 09](https://download.iru.cl/utal/org-est-2018-2/taller%2009.pdf)
- [Taller 10](https://download.iru.cl/utal/org-est-2018-2/taller%2010.pdf)
- [Taller 11](https://download.iru.cl/utal/org-est-2018-2/taller%2011.pdf)
- [Taller 12](https://download.iru.cl/utal/org-est-2018-2/taller%2012.pdf)
- [Taller 13](https://download.iru.cl/utal/org-est-2018-2/taller%2013.pdf)
- [Taller 14](https://download.iru.cl/utal/org-est-2018-2/taller%2014.pdf)
- [Taller 15](https://download.iru.cl/utal/org-est-2018-2/taller%2015.pdf)
- [Trabajo autónomo 01](https://download.iru.cl/utal/org-est-2018-2/trabajo%2001.pdf)
- [Trabajo autónomo 02](https://download.iru.cl/utal/org-est-2018-2/trabajo%2002.pdf)
- [Trabajo autónomo 03](https://download.iru.cl/utal/org-est-2018-2/trabajo%2003.pdf)
- [Trabajo autónomo 04](https://download.iru.cl/utal/org-est-2018-2/trabajo%2004.pdf)
- [Trabajo autónomo 05](https://download.iru.cl/utal/org-est-2018-2/trabajo%2005.pdf)
- [Trabajo autónomo 06](https://download.iru.cl/utal/org-est-2018-2/trabajo%2006.pdf)
- [Trabajo autónomo 07](https://download.iru.cl/utal/org-est-2018-2/trabajo%2007.pdf)
- [Trabajo autónomo 08](https://download.iru.cl/utal/org-est-2018-2/trabajo%2008.pdf)
- [Trabajo autónomo 09](https://download.iru.cl/utal/org-est-2018-2/trabajo%2009.pdf)
- [Trabajo autónomo 10](https://download.iru.cl/utal/org-est-2018-2/trabajo%2010.pdf)
- [Trabajo autónomo 11](https://download.iru.cl/utal/org-est-2018-2/trabajo%2011.pdf)
- [Trabajo autónomo 12](https://download.iru.cl/utal/org-est-2018-2/trabajo%2012.pdf)
- [Trabajo autónomo 13](https://download.iru.cl/utal/org-est-2018-2/trabajo%2013.pdf)
- [Trabajo autónomo 14](https://download.iru.cl/utal/org-est-2018-2/trabajo%2014.pdf)
- [Trabajo autónomo 15](https://download.iru.cl/utal/org-est-2018-2/trabajo%2015.pdf)

## Progreso
{{< progreso fuente="2018/2" syllabus="si" pruebas="no" guias="si" desc="no" >}}

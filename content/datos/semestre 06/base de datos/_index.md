+++
title = "Base de datos"
url = "/ramos/base-de-datos/"
+++
# Base de datos

## Fuente
"Kaula", más info [aquí](/informatica/kaula/).

## Archivos
- [01: Bases de datos I](https://download.iru.cl/utal/kaula/33/01.pdf)
- [02: Diseño Conceptual, Modelo Entidad/Relación](https://download.iru.cl/utal/kaula/33/02.pdf)
- [03: Diseño Lógico, Modelo Racional](https://download.iru.cl/utal/kaula/33/03.pdf)
- [04: Transformando MER a Modelo Relacional](https://download.iru.cl/utal/kaula/33/04.pdf)
- [05: Normalización](https://download.iru.cl/utal/kaula/33/05.pdf)
- [06: Introducción al álgebra relacional](https://download.iru.cl/utal/kaula/33/06.pdf)
- [07: SQL - Structured Query Language](https://download.iru.cl/utal/kaula/33/07.pdf)
- [08: Views (Vistas)](https://download.iru.cl/utal/kaula/33/08.pdf)
- [09: Funciones](https://download.iru.cl/utal/kaula/33/09.pdf)
- [10: Disparadores/Triggers](https://download.iru.cl/utal/kaula/33/10.pdf)

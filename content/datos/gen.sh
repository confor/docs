#!/bin/bash

i=0

while IFS= read -r line; do
  if [[ "$line" == Semestre* ]]; then
    i=$((i+1))
    continue
  fi
  
  if [[ "$line" == "" ]]; then
    continue
  fi
  
  number="$(printf '%02d' "$i")"
  folder="$(<<< "$line" sed 's/Á/á/g;s/É/é/g;s/Í/í/g;s/Ó/ó/g;s/Ú/ú/g' | tr '[:upper:]' '[:lower:]')"
  slug="$(<<< "$line" sed 's/á/a/g;s/é/e/g;s/í/i/g;s/ó/o/g;s/ú/u/g;s/Á/A/g;s/É/E/g;s/Í/I/g;s/Ó/O/g;s/Ú/U/g' | tr '[:upper:]' '[:lower:]' | tr ' ' '-')"
  
  echo "Processing $number $folder"
  
  mkdir -p "semestre $number/$folder/"
  touch "semestre $number/$folder/_index.md"
  
  {
    printf '+++\n'
    printf 'title = "%s"\n' "$line"
    printf 'url = "/ramos/%s/"\n' "$slug"
    printf '+++\n'
    printf '# %s\n\n' "$line"
  } > "semestre $number/$folder/_index.md"

done < list.txt

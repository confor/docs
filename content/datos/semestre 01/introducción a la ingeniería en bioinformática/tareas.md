+++
title = "Introducción a la ingeniería en bioinformática"
url = "/ramos/introduccion-a-la-ingenieria-en-bioinformatica/tareas/"
+++
# Introducción a la ingeniería en bioinformática
Artículo principal en [Introducción a la bioinformática](/bioinformatica/intro-a-la-bioinformatica/).

Las tareas son fáciles pero tediosas. Varían entre cuestionarios (alrededor de 4 preguntas), sacar capturas de pantalla, abrir una molécula en `vmd`, ver una charla ted (video/documental), o escribir scripts en bash.

El profe da las mismas 16 tareas todos los años.

## Tarea 01
El profesor pide:
- resumir el contenido de charlas media plana:
   - Jack Horner: Building a dinosaur from a chicken ([link](http://www.ted.com/talks/jack_horner_building_a_dinosaur_from_a_chicken.html))
   - Hendrik Poinar: Bring back the woolly mammoth! ([link](http://www.ted.com/talks/hendrik_poinar_bring_back_the_woolly_mammoth))
   - Juan Enriquez: The life code that will reshape the future ([link](http://www.ted.com/talks/juan_enriquez_on_genomics_and_our_future.html))
   - Alan Russell: The potential of regenerative medicine ([link](https://www.ted.com/talks/alan_russell_on_regenerating_our_bodies))
   - David Agus: A new strategy in the war on cancer (2010) ([link](https://www.ted.com/talks/david_agus_a_new_strategy_in_the_war_on_cancer))
- explicar en 150 palabras el más entretenido
- explicar en 100 palabras el rol de la bioinformatica en los videos vistos

### Charla 1: Dinosaurio de gallina
Jack Horner describe cómo su sueño de ser un paleontólogo y tener un dinosaurio de mascota le llevó a este punto en su vida: ha encontrado los primeros huevos de dinosaurio en una montaña, bebés en nidos, los primeros embriones y también acumulaciones masivas de huesos.

Debido a estos descubrimientos, Jack explica que interesantes hipótesis aparecieron junto a sus colegas: pudieron decir que los dinosaurios construían nidos y vivían en colonias, cuidando y alimentando a sus crías, viajando en enormes manadas y generalmente siendo seres muy sociales. Además evidencia sugiere que los dinosaurios cambiaban de apariencia desde su juventud hacia su adultez.

Por esta característica social, Michael Crichton habló en su libro de traer a la vida a los dinosaurios, mientras que Steven Spielberg en "Jurassic Park" demostraba posibles consecuencias.

Intentaron extraer ADN del hueso del muslo de un Tiranosaurio Rex, pero sólo encontraron hemo y estructuras interesantes, nada de ADN. Años después, otro Tiranosaurio apodado B-Rex tenía rasgos destacables que permitieron determinar que era una hembra que murió a los 16 años.

Al botar el hueso en ácido, pensaron que por estar fosilizado no quedaría nada, pero resultaron vasos sanguíneos y evidencia de proteínas. Aunque muestras frescas mostraban mejores tejidos, nada contenía ADN.

Por otro lado, se recuerda que las aves son dinosaurios, y que se podría usar transgénesis o selección para hacerlos más llamativos. Recientemente hemos podido estimular los genes de una gallina para que tenga dientes, entonces podríamos explotar atavismos y modificación genética para darle colas, manos en vez de alas, u otras características de dinosaurios.

Entonces, lo que se intenta de hacer es una gallina modificada, que resulte en un dinosaurio, para así enseñarle al mundo y a los niños de la biología evolutiva y el desarrollo de varias cosas.

### Charla 2: Mamut
En esta charla se habla de revivir al mamut, particularmente de los restos de mamuts lanudos preservados en el permahielo. Aunque hace muchos años parecía imposible secuenciar genomas de animales extintos, hoy si podemos traer a esos gigantes y peludos mamuts gracias a fósiles rescatables en Siberia y Alaska, donde su preservación es fenomenal: aún tienen dientes, huesos, algo que parece sangre, pelo y cabezas con cerebros adentro.

Debido a que la preservación de ADN es consistente con la temperatura a la que se preserva el cadáver, el único factor que está deconstruyendo el ADN del mamut son las bacterias que una vez estuvieron en simbiosis con el animal vivo, por lo que ahora el ADN está partido en pequeños fragmentos con información muy esparcida que hay que juntar como un rompecabezas. Gracias a la tecnología de punta, podemos rescatar todo el ADN que hay dentro de un hueso o diente, pero saldrá el del mamut mezclado con bacterias, y todo lo que vivió junto al ambiente del mamut, como bacterias, hongos u otros animales. Se puede esperar que el ADN obtenido sea al menos 50% de mamut.

A pesar de esto, tenemos ideas que nos dejan discriminar y capturar el ADN de mamut versus el ADN de otras cosas, y así bioinformáticamente obtener y reagrupar todos estos pequeños fragmentos aplicándolos en un cromosoma de un elefante Asiático o Africano. Tras esto obtenemos todos los puntos en los que se diferencia un elefante y un mamut.

Ya tenemos el genoma de un mamut casi completado, y tiene casi el doble de genes que un humano. Con toda esta información podemos tomar un cromosoma de elefante, modificarlo con la información de un mamut y luego de un arduo proceso traer de vuelta a algo que parezca un mamut.


### Charla 3: el ADN
La charla de Juan Enriquez menciona el importante progreso logrado en el estudio de la genética. Entendemos que el gen de un organismo son todas las modificaciones que ha sufrido, un rastro de su pasado escrito en cada parte del ADN, cómo lo podemos explotar para modificaciones para nuestro beneficio, y cómo entender el código del que estamos hechos traería cambios en el mercado, tecnología, política y sociedad.

### Charla 4: medicina regenerativa
La medicina regenerativa tiene el potencial de ayudar al cuerpo humano en regeneración o cura de lugares que son naturalmente difíciles, como tejidos perdidos en una quemadura u órganos que están fallando. Consiste en simplemente acelerar la velocidad de regeneración del cuerpo humano a un nivel clínicamente relevante.

Cuando tenemos una enfermedad debilitante como diabetes, cáncer, enfermedad de alzheimer o parkinson, falla de corazón o pulmones, sabemos que hay relativamente poco para poder curarnos, pero el Dr. Alan Russell propone ¿por qué no crear tejidos y órganos completamente nuevos para reemplazar los enfermos? La idea de curar enfermedades con dispositivos y drogas podría ser completamente olvidada si regeneramos toda la función del órgano o tejido dañado.

Uno de los problemas del sistema propuestos por Dr. Alan es que la "mejora de vida" y "reducción de pobreza" para alargar la longevidad significan que hay más tiempo en la vida de una persona para más enfermedades, y son cada vez más serias y más caras para tratar. La relación entre el dinero de una persona y su salud es obvia, la gente con más dinero vive más porque se puede sanar.

Otra cosa más importante e interesante es el diagnóstico de una enfermedad antes de su progresión, sería ideal tratar el problema cuando está naciendo y no cuando ya se tienen síntomas. Por ejemplo con la diabetes: diagnosticamos síntomas y cuando son obvios tratamos con insulina por 10, 20, 40 años o más, y es un tratamiento válido, pero eventualmente deja de funcionar. La idea es darle al páncreas la capacidad de regeneración muy temprano en la enfermedad, ojalá antes de que empiece a dar síntomas, y aunque sea caro, se puede lograr algo realmente diferente.

Actualmente la salamandra puede regenerar sus extremidades, y nosotros tenemos una capacidad general en el feto, incluso niños crecen sus dedos si les ocurren accidentes, pero perdemos esta habilidad al crecer. Podemos rescatar esta habilidad con la medicina regenerativa: ya se está usando y se ha usado en más de 400 000 pacientes, además de 11 000 niños que vuelven de guerras.

Las células madres también son el principal remedio para la medicina regenerativa, y resulta que en los extractos de liposucción se pueden encontrar muchas de estas, teniendo el efecto de quitarnos grasa para curarnos.


### Charla 5: cáncer
En el 2004, un titular describiendo una guerra fallida contra el cáncer era el panorama en la farmacia del hospital del Dr. David Agus. Obviamente era desmotivante, pero al leer el artículo uno podía darse cuenta de que el autor sobrevivió una lucha contra el cáncer gracias a recibir una terapia experimental para la enfermedad de Hodgkin.

Lo último en tecnología de diagnóstico de cáncer consiste en comparar cosas anormales con cosas normales. No hay prueba molecular, ni secuenciación de genes, ni observación elaborada de cromosomas. Todo lo que tenemos para diagnosis es esperar que haya una anomalía o un tumor, sacar una muestra con una aguja y analizarlo: nada más. Entre 1950 y 2001 no se ha encontrado una solución y las tasas de mortalidad se han mantenido iguales.

Recién cuando un paciente empieza a tener síntomas como cansancio, hinchazón, dolores o molestias realizamos que algo anda mal, y al momento de tomar una tomografía el cáncer ya está presente, por ejemplo en una masa en el hígado, quizás en otras partes. Esto es todo lo que podemos hacer: mirar los resultados de una enfermedad ya presente, y dado que el cáncer avanzado no es realísticamente curable, deberíamos entonces ubicarnos en la detección temprana, así luchar y prevenirlo antes de que aparezca. Es difícil de tratar, muy agresivo, pero si entendemos que no es sólo un defecto molecular, sino algo más, llegaremos a un nuevo método de tratarlo.

El reto del Dr. David Agus no es entender el cáncer, eso parece haber sido el camino de muchos durante las últimas cinco décadas, pero en vez de luchar para entenderlo deberíamos luchar para controlarlo, una estrategia muy diferente.

Ahora ya tenemos la tecnología para manejar cánceres individuales: en vez de ir a la clínica de cáncer de mama, se iría a la clínica de HER2 amplificado, o a una clínica de EGFR activado, y van a ver las lesiones patogénicas que causaron este cáncer. Ojalá que pasemos del arte de la medicina a la ciencia de la medicina, para poder tratarlo similar a las infecciones: "Este antibiótico tiene sentido, porque esta bacteria particular responderá a este". Cuando uno se expone a H1N1, tomar Tamiflu reduce drásticamente la severidad de los síntomas y previene muchas manifestaciones de la enfermedad, pero, ¿por qué? Porque sabemos lo que tienes y cómo tratarlo: exactamente lo que hace falta en el cáncer.


## Tarea 02
- buscar algo en la biblioteca (en la página web de la u)
- describir lo que es un paper
- buscar algo en pubmed. esta tarea no se entiende, el 2017 ya estaba __extremadamente desactualizada__

### Códigos de biblioteca
Para la primera pregunta hay que ir a la biblioteca a descifrar el sistema de organización que usan para los libros. Esto no tiene mucho sentido porque se pueden encontrar en [biblioteca.utalca.cl](http://biblioteca.utalca.cl/). El buscador se llama "metacatálogo primo" y está al final de la página a la derecha. Se puede buscar cualquier palabra y ahí aparecen los códigos de los libros. Intenta con "biologia" o "calculo".
```
Biopsicología
Pinel, John P. J.
BC-CG 612.8 P651p4E c.1

1001 videojuegos a los que hay que jugar antes de morir
Mott, Tony.
BC-BCCG2 794.8 C569v1E 2011 c.1

Manual de BORLAND C++ 4.0
Pappas, Chris H. / Murray, William H.
BC-BCCG2 005.133/BC++ P218b4E c.1

Handbook of child psychology and developmental science.
M. Lerner, Richard / Overton, Willis F. / Molenaar, Peter C. M.
BC-BCCG2 155.4 H236c 2012 v.1 c.1

Malezas de Chile
Ramirez de Vallejo, Adriana
BC-BCCG1 632.58 R173m

Cálculo
Kitchen, Joseph W.
BC-CG 515 K62c.E

Bioinformatics and molecular evolution
Higgs, Paul G. / Attwood, Teresa K.
BC-CG 572.8 H635b

Derecho romano
Iglesias, Juan
BCCG 340.54 I24d c.1 13a.ed.

Precálculo: funciones y gráficas
Barnett, Raymond A. / Ziegler, Michael R. / Byleen, Karl E.
BC-CG1 515 B261p c.1

El lenguaje de programación java
Arnold, Ken / Gosling, James / Holmes, David
BC-CG2 005.133 A757j3E

Las neuronas espejo: Los mecanismos de la empatía emocional
Rizzolatti, Giacomo / Sinigaglia, Corrado
BC-CG2 159.942 R627q.E

Elementos de biología celular y genética
Spotorno, A
BCCG 574.87 E38b 2a.ed.
```

### Paper
Un artículo científico (o paper) es una publicación cuya intención es avanzar el progreso de la ciencia reportando nuevas investigaciones o resultados de teorías de manera clara y precisa. También busca fomentar el desarrollo de métodos experimentales innovadores. Generalmente se publican en revistas enfocadas a la ciencia (Nature, Science) o en sistemas en línea (PubMed/Medline).

Los artículos científicos han pasado por muchas iteraciones hasta el punto de haber estandarizado el formato: virtualmente todos llevan un título, resumen, introducción, materiales y métodos, resultados, discusión, conclusión, y a veces referencias o reconocimientos.

El título claramente indica el problema investigado y variables principales de manera concisa y fácil de entender, seguido de los autores del escrito. El resumen o abstracto entrega el contenido de manera breve, rápida y exacta, conteniendo el objetivo o hipótesis en el primer párrafo, seguido de la metodología y razones del estudio. La introducción presenta el qué y porqué de la investigación, incluyendo el planteamiento, objetivo y preguntas del problema, además de la justificación y contexto de la investigación (¿cómo?, ¿dónde?), las variables a estudiar y sus definiciones. Debe ser llamativo e invitar al lector a seguir leyendo. La sección de metodología explica los pasos que fueron llevados a cabo para completar el experimento, considerando también a los materiales, quienes deben ser descritos.

Lo más importante del artículo científico son los resultados: deben presentarse clara, precisa y concisamente en el orden planteado sin comentarios, ni juicios, ni justificaciones, con una secuencia lógica. Se debe focalizar hacia los hallazgos relevantes y respondiendo a la pregunta de investigación.

La discusión es encargada de discutir los resultados propios y más importantes, debatiendo la conclusión para convencer al lector de que los resultados tienen validez. Su estilo argumentativo puede contrastar con el descriptivo de las otras secciones. La conclusión muestra la respuesta a la interrogante que generó el estudio y a los objetivos planteados.

Referencias:
- [How do I write a scientific paper? SciDev.Net](https://www.scidev.net/global/publishing/practical-guide/how-do-i-write-a-scientific-paper-.html)
- [ELABORACIÓN DE UN ARTÍCULO CIENTÍFICO DE INVESTIGACIÓN](https://scielo.conicyt.cl/scielo.php?script=sci_arttext&pid=S0717-95532004000100003)

### Buscar en pubmed
El profe pide buscar en pubmed cosas como "dentista", "cancer", "herpes", pero su `.ppt` con las instrucciones no tiene sentido porque no ha sido actualizado hace años y la página ha cambiado varias veces.

Suerte!


## Tarea 03
- ¿Qué es una base de datos?
- Nombre y explique sus características
- Describa 5 bases de datos

Las características son cosas como orden, fácil acceso, habiliad para filtrar, etc. Ejemplos son cosas como PubMed, IMDB, esta misma página, MercadoLibre...

### Definición de base de datos
Una base de datos es una colección de información organizada sistemáticamente, de manera que quienes administran o interactuen con esta puedan utilizar y procesar su información utilizando filtros y obteniendo resultados de manera más eficiente. Con la ayuda de computadores, las bases de datos digitales permiten acceder a la información de manera casi instantánea.


## Tarea 04
- ¿Qué almacena PubMed?
- Buscar un virus cualquiera en PubMed e indicar si permite bajar el documento
- Seleccionar un paper cualquiera y resumirlo

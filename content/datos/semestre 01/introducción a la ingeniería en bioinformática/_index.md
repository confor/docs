+++
title = "Introducción a la ingeniería en bioinformática"
url = "/ramos/introduccion-a-la-ingenieria-en-bioinformatica/"
+++

- Unidad I
  - Qué es la bioinformática
  - Uso de tecnología
  - Qué son las bases de datos
  - Cómo usar páginas webs como pubmed, pubchem, etc
- Unidad II
  - Conocer la genómica y metagenómica ("Dogma central de la genómica")
  - Idea general de lo que es alineamiento de secuencias
  - Servicios web para comparar secuencias biológicas
- Unidad III
  - Cómo usar un computador para mirar moléculas (básicamente VMD)
  - Servicios con información de moléculas
  - El rol de bioinformática en la producción de fármacos
  - Charlas y mucho blablabla

## Evaluaciones
El ramo tiene tres pruebas de las tres unidades, pero un montón de prácticos a lo largo de las clases.

Las pruebas son convencionales (lapiz y papel) y llevan una sección de alternativas y otra de desarrollo. Es literalmente la materia vista en clase, no hay que pensar ni analizar mucho. Con prestar atención en clases es suficiente y no hay que estudiar.

Los prácticos se repiten todos los años. El profesor recicla el mismo `pdf` con instrucciones que ya no sirven porque los programas y servicios (páginas web) han cambiado mucho. Las tareas se hacen en clases.

{{< hint warning >}}
Ramo por competencia. Si repruebas una unidad, repruebas el ramo
{{< /hint >}}

## Tareas
El profe da las mismas 16 tareas todos los años. [Ver las tareas desarrolladas __aqui__](/ramos/introduccion-a-la-ingenieria-en-bioinformatica/tareas/).

## Apuntes desordenados
- gestión de información
  - soluciona el exceso de información
  - resuelve la necesidad de filtrar la información para entenderla
  - hay que saber encontrar fuentes confiables de info
- saber manejar información es importante en biología
  - hay que procesar enormes cantidades de datos
  - deben ser navegados, filtrados y minados (limpiar y normalizar)
  - minar información sirve para concluir cosas
- cosas importantes en bioinformatica
  - bioquimica
  - probabilidad y estadistica
  - informatica
- bases de datos
  - deben ser creadas y administradas
  - sirven para extraer e interpretar datos especificos
  - se le aplican algoritmos y funciones para sacar información en la que inferir conclusiones

### Unidad 01
- herramientas unix
  - linea de comandos
  - emulador de terminal
  - editores: vi, vim, nano
  - comandos: mv, cp, cd, dir, md, ls, rm, grep, awk, ...
- cómo usar google (si, en serio)
- bases de datos
  - tenemos que buscar un auto barato (parece una broma, pero no)
  - tipos de bases de datos:
    - bibliograficas
    - numericas
    - de texto completo
    - multimedia (banco de imagenes, audio, video)
    - especializada (ej: secuencias biológicas, moléculas)
- revistas cientificas
  - peer review es importante
  - son su propia fuente de información
  - de aquí vienen los "papers"
  - tienen la info más reciente (publicaciones semanales o mensuales)
- repositorios
  - bases de datos de bases de datos

## Progreso
{{< progreso fuente="2018/1" syllabus="no" pruebas="no" guias="no" desc="si" >}}

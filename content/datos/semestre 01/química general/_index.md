+++
title = "Química general"
url = "/ramos/quimica-general/"
+++

El ramo es una introducción muy liviana a la química.

- Unidad I: Estructura atómica. Nomenclatura inorgánica.
- Unidad II: Estequiometría.
- Unidad III: Soluciones. Equilibrio químico. pH y óxido-reducción.

Este ramo usó "Química: La ciencia central" de Brown LeMay Bursten (9na o 11va ed.)

## Fuente
Ing. civil en Bioinformática, 2018 / 1.

## Evaluaciones
Hay dos tipos de evaluaciones: prueba convencional (lapiz y papel) y laboratorio. Las pruebas corresponden a las tres unidades, y las evaluaciones de laboratorio son semanales (repetidas todo el semestre).

Las pruebas son hechas con ejercicios de las guias, es decir, **las guias son las pruebas**. Si haces y entiendes las guias, es seguro que te irá bien (pregunta dudas al profesor!).

Los controles de laboratorio son pruebas cortas (2 a 4 preguntas) que evalúan lo que se hará en el correspondiente práctico. Se puede estudiar con la guía de laboratorio, leyendo la sección que corresponde al práctico que elija el profesor.

Los informes de laboratorio se deben entregar al siguiente laboratorio después de hacer un práctico. El profesor es **muy** estricto con el formato y la conclusión, pero es flojo para revisar las introducciones.

En el caso de tener un práctico en laboratorio, el profesor exige llevar ciertas cosas. Al no tener uno de estos, el profesor no permitirá entrar al laboratorio.
- guia (del práctico correspondiente)
- delantal
- guantes y lentes (cuando sea necesario, ej: práctico con ácidos)

Las pruebas y controles de laboratorio parecen ser diferentes todos los años, pero los prácticos e informes son los mismos.

Los informes tienen el siguiente formato que debes respetar:
- Inicio
  - título
  - autoría: nombre completo del alumno, profesor(es) y carrera
  - logo de la universidad (generalmente en una esquina)
  - fecha
  - resumen: sintetiza el contenido del informe
  - palabras clave: 2 a 6 palabras que describen el contenido. por ejemplo, para "soluciones y diluciones" pueden ser "concentración, soluto, solvente, molaridad, molalidad"
- Introducción
  - explicación que responda a "¿cuál es el problema?"
  - objetivos del experimento ("¿por qué hago esto?")
  - marco teórico (opcional)
- Metodología
  - materiales: lista de **todo** lo usado en el práctico. deben ir todos los materiales, equipo, reactivos y reactantes usados. si se usó algo que no estaba en la guia del profesor, añadirlo al informe
  - procedimiento: instrucciones del montaje del práctico. si copias de la guia, fijate en errores intencionales (ver "Notas" más abajo)
- Resultados
  - datos objetivos obtenidos a partir del experimento
  - tablas o gráficos, si es necesario
- Conclusión
  - discusión: un análisis crítico del práctico realizado. intenta de responder a "¿por qué ocurrió esto?", "¿cual es la causa de X cosa?" y se apoya fuertemente en bibliografía. precaución de no repetir lo mismo que está en los resultados. aquí se tiene que cuestionar el *por qué* de lo que pasó e intentar responderlo con bibliografía.
  - conclusión
- Bibliografía
  - lista de libros, páginas web, videos, revistas, etc usados en el informe. no olvides que la guia del profesor (el manual del ramo) también es bibliografia.


## Notas
- Informes: usar lenguaje neutro, nunca *tu*tear
  - ej: "se usó X cosa" en vez de "usé X cosa"
- Informes: el profe revisa **muy estrictamente** la sección de resultados y conclusión, sobre todo discusión
- Datos: hay que tener cuidado al escribir los datos. hay que poner atención a espacios, puntuación, digitos significativos y la unidad. Por ejemplo, volumen es `5 mL`, NO `5ml` o `5 ml`. Siempre un espacio entre número y unidad, cuidando además mayusculas y minúsculas.

El profe de este ramo es siempre el mismo. Es un amor de persona y responde cualquier duda, pero a veces es difícil entenderle porque mezcla español con inglés y portugués. Si uno no entiende algo siempre le puede preguntar, sobre todo antes de una prueba.

## Archivos
- [Guia 1.1](https://download.iru.cl/utal/quimica-general-2018-1/Guia1.1.pdf)
- [Guia 1.2](https://download.iru.cl/utal/quimica-general-2018-1/Guia1.2.pdf)
- [Guia 2](https://download.iru.cl/utal/quimica-general-2018-1/Guia2.pdf)
- [Guia 3](https://download.iru.cl/utal/quimica-general-2018-1/Guia3.pdf)
- [Manual](https://download.iru.cl/utal/quimica-general-2018-1/Manual%20Laboratorio.pdf)
- [Syllabus](https://download.iru.cl/utal/quimica-general-2018-1/syllabus.pdf)

## Progreso
{{< progreso fuente="2018/1" syllabus="si" pruebas="no" guias="si" desc="si" >}}

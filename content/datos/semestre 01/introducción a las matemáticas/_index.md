+++
title = "Introducción a las matemáticas"
url = "/ramos/introduccion-a-las-matematicas/"
+++

Es un ramo de nivelación, más o menos contenido desde sexto básico hasta cuarto medio.

- Unidad 1: Números reales. Exponentes y radicales.
- Unidad 2: Ecuaciones. Desigualdades.
- Unidad 3: Geometría analítica. Trigonometría.

Este ramo usó "Precálculo: Matemáticas para el cálculo" de James Stewart, 6ta edición. Puedes encontrarlo en [libros](/libros).

Toda la materia del ramo corresponde al capítulo 1 y 6 del libro.

## Fuente
Ing. civil en Bioinformática, **2018 / 1**.

## Evaluaciones
El ramo tuvo evaluaciones convencionales (lapiz y papel) de las tres unidades, y un exámen final con toda la materia.

## Archivos

- [Errores comunes](http://download.iru.cl/utal/intro-matematicas-2018-1/ErroresComunes.pdf)
- [Syllabus](http://download.iru.cl/utal/intro-matematicas-2018-1/Syllabus.pdf)

Las guias son extractos de los capítulos del libro.

- [Guia 1.1](http://download.iru.cl/utal/intro-matematicas-2018-1/Guia1.1.pdf)
- [Guia 1.2](http://download.iru.cl/utal/intro-matematicas-2018-1/Guia1.2.pdf)
- [Guia 1.3](http://download.iru.cl/utal/intro-matematicas-2018-1/Guia1.3.pdf)
- [Guia 1.4](http://download.iru.cl/utal/intro-matematicas-2018-1/Guia1.4.pdf)
- [Guia 1.5](http://download.iru.cl/utal/intro-matematicas-2018-1/Guia1.5.pdf)
- [Guia 1.6](http://download.iru.cl/utal/intro-matematicas-2018-1/Guia1.6.pdf)
- [Guia 1.7](http://download.iru.cl/utal/intro-matematicas-2018-1/Guia1.7.pdf)
- [Guia 1.8](http://download.iru.cl/utal/intro-matematicas-2018-1/Guia1.8.pdf)
- [Guia 1.10](http://download.iru.cl/utal/intro-matematicas-2018-1/Guia1.10.pdf)
- [Guia 1.11](http://download.iru.cl/utal/intro-matematicas-2018-1/Guia1.11.pdf)
- [Guia 6.1](http://download.iru.cl/utal/intro-matematicas-2018-1/Guia6.1.pdf)
- [Guia 6.2](http://download.iru.cl/utal/intro-matematicas-2018-1/Guia6.2.pdf)
- [Guia 6.3](http://download.iru.cl/utal/intro-matematicas-2018-1/Guia6.3.pdf)

## Progreso
{{< progreso fuente="2018/1" syllabus="si" pruebas="no" guias="si" desc="si" >}}

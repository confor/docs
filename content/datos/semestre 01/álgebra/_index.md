+++
title = "Álgebra"
url = "/ramos/algebra/"
+++

## Contenidos
- Unidad I: Lógica. Teoría de conjuntos.
- Unidad II: Matemática discreta. Números complejos.
- Unidad III: Teoría de polinomios. Introducción a la programación lineal. Cónicas.

## Fuente
Ing. civil en Bioinformática, 2018 / 1.

## Evaluaciones
El ramo tuvo evaluaciones convencionales (lapiz y papel) de las tres unidades y un exámen final.

## Archivos
Todo el contenido del ramo fue enseñado con plumón y pizarra. No hay presentaciones (pdf/ppt).

- [Guia 01](http://download.iru.cl/utal/algebra-2018-1/Guia1.pdf)
- [Guia 02](http://download.iru.cl/utal/algebra-2018-1/Guia2.pdf)
- [Guia 03](http://download.iru.cl/utal/algebra-2018-1/Guia3.pdf)
- [Guia 04](http://download.iru.cl/utal/algebra-2018-1/Guia4.pdf)
- [Guia 05](http://download.iru.cl/utal/algebra-2018-1/Guia5.pdf)
- [Guia 06](http://download.iru.cl/utal/algebra-2018-1/Guia6.pdf)
- [Guia 07 (con materia)](http://download.iru.cl/utal/algebra-2018-1/Guia7%20(con%20materia).pdf)
- [Guia 08](http://download.iru.cl/utal/algebra-2018-1/Guia8.pdf)
- [Syllabus](http://download.iru.cl/utal/algebra-2018-1/syllabus.pdf)

## Progreso
{{< progreso fuente="2018/1" syllabus="si" pruebas="no" guias="si" desc="no" >}}

+++
title = "Taller de programación web"
url = "/ramos/taller-de-programacion-web/"
+++
# Taller de programación web

## Fuente
"Kaula", más info [aquí](/informatica/kaula/).

## Archivos
- [Tecnologías web](https://download.iru.cl/utal/kaula/55/1490017440tema1.pdf)
- [HTML y CSS](https://download.iru.cl/utal/kaula/55/1490023936tema2.pdf)
- [Apache web Server](https://download.iru.cl/utal/kaula/55/1490033802tema3.pdf)
- [SGBD PostgreSQL](https://download.iru.cl/utal/kaula/55/1490042520tema4.pdf)
- [PHP](https://download.iru.cl/utal/kaula/55/1490045468tema5.pdf)

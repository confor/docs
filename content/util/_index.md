+++
title = "Utilidades"
weight = 2
+++
# Utilidades

## Buscar funcionarios
__[Link](buscar-funcionario/)__

Página para buscar entre los muchos funcionarios de la universidad, incluyendo profesores. Puedes encontrar:
- nombre completo
- lugar de trabajo
- correo electrónico __(!)__
- teléfono

## Jitsi Meet
__[Link](https://meet.jit.si/)__

Videollamadas grupales gratuitas
- sin límite de tiempo
- sin registro
- sólo comparte el enlace y otros se pueden unir
- funciona en windows, linux, macos, android o iphone
- puedes compartir pantalla

Puedes usar otra instancia:
- [Argentina (jitsi.hostcero.com)](https://jitsi.hostcero.com/)
- [Chile (meet.overland.cl)](https://meet.overland.cl/)
- [Chile (vc.sanclemente.cl)](https://vc.sanclemente.cl/)
- [Estados Unidos (meet.jit.si)](https://meet.jit.si/)
- [Otros](https://github.com/jitsi/jitsi-meet/wiki/Jitsi-Meet-Instances)

## Etherpad
__[Link](https://pad.disroot.org/)__

Editor colaborativo en tiempo real. Es como un documento de Google Drive pero simple.
- edita texto junto a otras personas
- sólo comparte el enlace y otros se pueden unir
- guarda como documento de word, PDF u otros
- no necesita registro ni identificación

Puedes encontrar otras instancias [aquí](https://github.com/ether/etherpad-lite/wiki/Sites-that-run-Etherpad-Lite)
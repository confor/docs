+++
title = "Aportes"
weight = 2
+++
# Aportes
Todo el texto de esta página está disponible en [este repositorio](https://gitlab.com/confor/docs).

Si tienes archivos grandes que aportar como pruebas, libros, guias o apuntes, puedes encontrar más información sobre cómo subir estos archivos al servidor al preguntar en cierto grupo de WhatsApp que tiene muchos alumnos.

**¡Muchas gracias a todos los que han compartido contenido!**